# syncr

Alternative sync tool for EGI AppDB image lists. No configuration, no options, just sync. As is.

## Requirements
* Installed deps
```
curl jq qemu-img openssl tar file tr cut wc sha1sum mktemp openstack
```
* Installed IGTF Trust Anchors in `/etc/grid-security/certificates`
* AppDB Personal Access Token (included in every image list URL)
* Configured image list entries in `lists.txt`
```
VO_NAME1 IMAGE_LIST_URL_W_ACCESS_TOKEN1
VO_NAME2 IMAGE_LIST_URL_W_ACCESS_TOKEN2
VO_NAME3 IMAGE_LIST_URL_W_ACCESS_TOKEN3
```
* Configured OpenStack CLI (`ENV` or `clouds.yaml`) for `admin` access

## Usage
```bash
# Prepare ENV.
mkdir tmp
touch tmp/skip

# Exclude known trouble makers by URL.
echo "https://do/not/download/image/with/this/url" > tmp/skip

# Run, syncr, run!
./syncr.sh lists.txt `pwd`/tmp
```
