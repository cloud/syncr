#!/bin/bash

# -------------------------------------------------------------------------- #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
#--------------------------------------------------------------------------- #

set -o pipefail

# Sort-of configuration
SYNCR_LIST=$1                               # path to file, formatted as "VO_NAME LIST_URL", one per line
GRID_CA_DIR=/etc/grid-security/certificates # IGTF trust anchors in PEM format, one CA per file
TMP_DIR=${2:-/tmp/syncr}                    # workdir, with lots of space
SKIP_FILE=${3:-$TMP_DIR/skip}               # path to file with URLs to exclude from processing

IMAGE_ATTR_EXTRACTION="dc:description dc:identifier ad:appid ad:mpuri ad:base_mpuri dc:title hv:uri hv:version sl:checksum:sha512"
IMAGE_PROPERTY_PREFIX="CLOUDKEEPER_"
OS_PROJECT_BASE_DOMAIN="egi_eu"

IMAGE_STATIC_PROPERTIES="--property hw_scsi_model=virtio-scsi"
IMAGE_STATIC_PROPERTIES="$IMAGE_STATIC_PROPERTIES --property hw_disk_bus=scsi"
IMAGE_STATIC_PROPERTIES="$IMAGE_STATIC_PROPERTIES --property hw_rng_model=virtio"
IMAGE_STATIC_PROPERTIES="$IMAGE_STATIC_PROPERTIES --property hw_qemu_guest_agent=yes"
IMAGE_STATIC_PROPERTIES="$IMAGE_STATIC_PROPERTIES --property os_require_quiesce=yes"
IMAGE_STATIC_PROPERTIES="$IMAGE_STATIC_PROPERTIES --property os_type=linux"

# Helper functions
## $1 log level for `logger`
## $2 message to log under chosen level
function log () {
  CLEAN_OUT=$(echo "${2}" | sed 's/\/\/.*:x-oauth-basic@/\/\//')
  logger -i -s -p "${1}" "${CLEAN_OUT}"
}

## $1 message to log under level `ERROR`
function log_err () {
  log error "${1}"
}

## $1 message to log under level `INFO`
function log_info () {
  log info "${1}"
}

## $1 message to log under level `DEBUG`
function log_debug () {
  log debug "${1}"
}

## $1 URL to download
## $2 output file for downloaded data
function download_url () {
  log_debug "Downloading URL \"$1\" to file \"$2\""
  curl -fs --capath "${GRID_CA_DIR}" -o "$2" "$1"

  if [ $? -ne 0 ]; then
    log_err "Attempt to download URL \"$1\" failed"
    exit 4
  fi
}

## $1 SMIME formatted message to verify, as a file
## $2 output file for message content with SMIME stripped
function check_smime () {
  log_debug "Checking signature on list \"$1\" and saving as \"$2\""
  openssl smime -verify -CApath "${GRID_CA_DIR}" -in "$1" -out "$2" > /dev/null 2>&1

  if [ $? -ne 0 ]; then
    log_err "Attempt to verify signature on \"$1\" failed"
    exit 5
  fi
}

## $1 image identifier
## $2 image list file
## $3 working directory for attribute storage
function extract_image_attributes () {
  IMAGE_ATTRS=$(jq -r ".\"hv:imagelist\".\"hv:images\"[] | select(.\"hv:image\".\"dc:identifier\" == \"$1\")" "$2")

  echo "$IMAGE_ATTRS" | jq -r ".\"hv:image\"" > "${3}/all.txt"

  for IMG_ATTR in $IMAGE_ATTR_EXTRACTION; do
    echo "$IMAGE_ATTRS" | jq -r ".\"hv:image\".\"${IMG_ATTR}\"" > "${3}/${IMG_ATTR}.txt"

    if [ $? -ne 0 ]; then
      log_err "Attempt to extract attribute \"$IMG_ATTR\" for \"$1\" failed"
      exit 11
    fi
  done
}

## $1 OVA file to extract disk image from, will be overwritten
## $2 workdir for image processing
function cherrypick_ova () {
  log_debug "Extracting first disk from OVA \"$1\" in dir \"$2\""
  IMAGE_FILE_TMP=$(mktemp -t -p "${2}")

  VMDK_DISK_COUNT=$(tar --list --file "$1" "*.vmdk" | wc -l)
  if [ $VMDK_DISK_COUNT -ne 1 ]; then
    log_err "OVA file \"$1\" contains more than one VMDK disk, not supported"
    exit 6
  fi

  IMAGE_VMDK_DISK=$(tar --list --file "$1" "*.vmdk")
  tar --extract --file "$1" --to-stdout "$IMAGE_VMDK_DISK" > "$IMAGE_FILE_TMP"

  if [ $? -ne 0 ]; then
    log_err "Attempt to extract disk from \"$1\" failed"
    exit 7
  fi

  mv "$IMAGE_FILE_TMP" "$1"

  if [ $? -ne 0 ]; then
    log_err "Attempt to move \"$IMAGE_FILE_TMP\" to \"$1\" failed"
    exit 8
  fi
}

## $1 disk image file in format other than `raw`, will be overwritten
## $2 workdir for image processing
function convert_to_raw () {
  log_debug "Converting disk \"$1\" to raw in dir \"$2\""
  IMAGE_FILE_TMP=$(mktemp -t -p "${2}")

  file -b "$1" | grep 'boot sector' 2>&1 /dev/null
  if [ $? -eq 0 ]; then
    log_debug "Disk \"$1\" is already raw, not converting"
  else
    log_debug "Converting disk \"$1\" to raw, tmp as \"$IMAGE_FILE_TMP\""
    qemu-img convert -O raw "$1" "$IMAGE_FILE_TMP"

    if [ $? -ne 0 ]; then
      log_err "Attempt convert \"$1\" to raw failed"
      exit 9
    fi

    mv "$IMAGE_FILE_TMP" "$1"

    if [ $? -ne 0 ]; then
      log_err "Attempt to move \"$IMAGE_FILE_TMP\" to \"$1\" failed"
      exit 10
    fi
  fi
}

## $1 file to read the attribute from
function read_image_attr () {
  if [ ! -f "$1" ] || [ ! -r "$1" ]; then
    log_debug "Attribute \"$1\" is not available"
    echo -n "N/A"
  fi

  cat "$1" | tr -d '\n'
}

## $1 VO name that will be used as a project name for upload
## $2 disk image file for upload to Glance, in `raw` format
## $3 directory with selected image attributes, see `$IMAGE_ATTR_EXTRACTION`
function upload_to_catalog () {
  log_debug "Uploading file \"$2\" as image with attrs from \"$3\" for VO \"$1\""

  IMAGE_NAME=$(read_image_attr "${3}/dc:title.txt")
  IMAGE_DESC=$(read_image_attr "${3}/dc:description.txt")
  IMAGE_MPURI=$(read_image_attr "${3}/ad:mpuri.txt")
  IMAGE_APPID=$(read_image_attr "${3}/ad:appid.txt")
  IMAGE_BMPURI=$(read_image_attr "${3}/ad:base_mpuri.txt")
  IMAGE_ID=$(read_image_attr "${3}/dc:identifier.txt")
  IMAGE_VERS=$(read_image_attr "${3}/hv:version.txt")
  IMAGE_ORIG_SHA512=$(read_image_attr "${3}/sl:checksum:sha512.txt")
  IMAGE_ALL=$(read_image_attr "${3}/all.txt")

  set -x
  OS_IMAGE_ID=$(
    openstack image create --file "$2" --project "$1" --project-domain "$OS_PROJECT_BASE_DOMAIN" \
                           --private --tag external --tag appdb --tag egi \
                           --disk-format raw --container-format bare \
                           --format value --column id \
                           --property ${IMAGE_PROPERTY_PREFIX}title="$IMAGE_NAME" \
                           --property ${IMAGE_PROPERTY_PREFIX}description="$IMAGE_DESC" \
                           --property ${IMAGE_PROPERTY_PREFIX}mpuri="$IMAGE_MPURI" \
                           --property ${IMAGE_PROPERTY_PREFIX}appid="$IMAGE_APPID" \
                           --property ${IMAGE_PROPERTY_PREFIX}base_mpuri="$IMAGE_BMPURI" \
                           --property ${IMAGE_PROPERTY_PREFIX}identifier="$IMAGE_ID" \
                           --property ${IMAGE_PROPERTY_PREFIX}version="$IMAGE_VERS" \
                           --property ${IMAGE_PROPERTY_PREFIX}digest="sha512" \
                           --property ${IMAGE_PROPERTY_PREFIX}checksum="$IMAGE_ORIG_SHA512" \
                           --property ${IMAGE_PROPERTY_PREFIX}vo="$1" \
                           --property vmcatcher_event_dc_description="$IMAGE_DESC" \
                           --property description="$IMAGE_DESC" \
                           --property vmcatcher_event_ad_mpuri="$IMAGE_MPURI" \
                           --property APPLIANCE_ATTRIBUTES="'"$IMAGE_ALL"'" \
                           $IMAGE_STATIC_PROPERTIES \
                           "$IMAGE_NAME"
  )
  set +x

  if [ $? -ne 0 ]; then
    log_err "Failed to upload image \"$IMAGE_ID\" for VO \"$1\""
    exit 12
  else
    log_info "Uploaded \"$IMAGE_ID\" for VO \"$1\" as \"$OS_IMAGE_ID\""
  fi
}

## $1 VO name of the image, maps to OS project name
## $2 directory with selected image attributes, see `$IMAGE_ATTR_EXTRACTION`
function check_image () {
  log_debug "Checking image with attrs in \"$2\" for VO \"$1\""

  IMAGE_ID=$(read_image_attr "${2}/dc:identifier.txt")

  set -x
  FOUND=$(openstack image list --property ${IMAGE_PROPERTY_PREFIX}identifier="$IMAGE_ID" --property ${IMAGE_PROPERTY_PREFIX}vo="$1" --tag egi --format value --column ID)
  set +x

  if [ -z "$FOUND" ]; then
    echo -n "no"
  else
    echo -n "yes"
  fi
}

## $1 name of the VO that owns this image
## $2 directory with selected image attributes, see `$IMAGE_ATTR_EXTRACTION`
## $3 workdir for image processing and tmp files
function handle_image () {
  log_debug "Handling image with attrs in \"$2\" for VO \"$1\" in dir \"$3\""

  EXISTS=$(check_image "$1" "$IMAGE_ATTR_DIR")
  if [ "x$EXISTS" = "xno" ]; then
    IMAGE_URL=$(read_image_attr "${2}/hv:uri.txt")
    IMAGE_DW="${3}/image"

    grep -F -e "$IMAGE_URL" "$SKIP_FILE" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      download_url "$IMAGE_URL" "$IMAGE_DW"

      file "$IMAGE_DW" | grep -F 'POSIX tar archive' > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        cherrypick_ova "$IMAGE_DW" "$3"
      fi

      convert_to_raw "$IMAGE_DW" "$3"
      upload_to_catalog "$1" "$IMAGE_DW" "$2"
    else
      log_debug "Skipping \"$IMAGE_URL\" as requested by skipfile \"$SKIP_FILE\""
    fi
  else
    log_info "Given image already uploaded for VO \"$1\""
  fi
}

## $1 name of the VO to process
## $2 URL pointing to the image list for the given VO
## $3 workdir for image processing and tmp files
function handle_list () {
  log_debug "Handling list \"$2\" for VO \"$1\" in dir \"$3\""

  IMAGE_LIST_SMIME="${3}/${1}.smime"
  IMAGE_LIST="${3}/${1}.json"

  download_url "$2" "$IMAGE_LIST_SMIME"
  check_smime "$IMAGE_LIST_SMIME" "$IMAGE_LIST"

  IMAGES=$(jq -r '."hv:imagelist"."hv:images"[] | ."hv:image"."dc:identifier"' "$IMAGE_LIST")
  for IMAGE in $IMAGES; do
    IMAGE_TMP_DIR="${3}/${IMAGE}"
    IMAGE_ATTR_DIR="${IMAGE_TMP_DIR}/attrs"
    mkdir -p "$IMAGE_ATTR_DIR"

    extract_image_attributes "$IMAGE" "$IMAGE_LIST" "$IMAGE_ATTR_DIR"
    handle_image "$1" "$IMAGE_ATTR_DIR" "$IMAGE_TMP_DIR"
    rm -rf "$IMAGE_TMP_DIR"
  done
}

# Check ENV sanity
if [ $(id -u) -eq 0 ]; then
  log_err "Cannot run syncr as root"
  exit 1
fi

if [ -z "$SYNCR_LIST" ]; then
  log_err "You have to provide path to list file"
  exit 2
fi

# Check necessary tools
TOOLS="curl jq qemu-img openssl tar file tr cut wc sha1sum mktemp openstack"
for TOOL in $TOOLS; do
  which "$TOOL" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    log_err "Tool $TOOL is not available, install it first"
    exit 3
  fi
done

# Check OS access
openstack catalog list > /dev/null 2>&1
if [ $? -ne 0 ]; then
  log_err "Could not access OpenStack API via client, check CLI configuration and/or ENV"
  exit 3
fi

# Read list file and iterate
while IFS= read -r LINE || [[ -n "$LINE" ]]; do
  log_debug "Processing line: \"$LINE\""

  VO_NAME=$(echo "$LINE" | tr -s ' ' | cut -d ' ' -f1)
  VO_LIST_URL=$(echo "$LINE" | tr -s ' ' | cut -d ' ' -f2)

  mkdir -p "$TMP_DIR"
  touch "$SKIP_FILE"

  LIST_TMP_DIR=$(mktemp -t -d -p "${TMP_DIR}")
  handle_list "$VO_NAME" "$VO_LIST_URL" "$LIST_TMP_DIR"
  rm -rf "$LIST_TMP_DIR"
done < "$SYNCR_LIST"
